<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyConfig extends BaseModel
{
    protected $table='company_config';
    protected $fillable=['company_name','slogan','address'];
    protected $rules=[
      'company_name'=>'string|required',
       'address'=>'string',
       'slogan'=>'string|required',

    ];

}
