<?php
namespace App\Http\Controllers;
use App\Member;
use App\Users;
use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;



class RegisterController extends Controller {


    public function edits($id){
       $item = Member::find($id,['id','fullname','dob','sex','fathers_name','marital_status','spouse_name','perm_address','temp_address','pan_no','introducer_name','introducer_id','spiller_name','spiller_id','parent_id','stockist_name','bank_ac_no','bank_name','mobile','color']);
       return $item;
     }


public function create(Request $request){
      $mem = new Member();
      $parentId=$request->input("parent_id",null);
      $branchValue=$request->input("branch",null);
     if($mem->validate($request->all())){
        $mem->fill($request->all());
         $mem->side=$branchValue;
            if($parentId==null){
              $mem->parent_id=0;
            }
            if(($totalBranch=Member::where('parent_id','=',$parentId)->count())>=2){
            return json_encode(['status'=>0,'title'=>"error",'text'=>"Parent Already Have Left And Right Member."]);
            }else if($totalBranch>0){
             $branchCheck=Member::where('parent_id','=',$parentId)->select("branch")->get(0);
            $branchVal=substr($branchCheck->first()->branch,-1);
             if($branchVal==$branchValue){

             	if($branchValue==1){
             		$bName="Left";
             	}else{$bName="Right";}

             	return json_encode(['status'=>0,'title'=>"error",'text'=>$bName." Branch Already Exist."]);
             }
            }

           $mem->save();

           $ownId=$mem->id;
           $this->createPath($parentId,$ownId);
           $this->createBranch($parentId,$branchValue,$ownId);
           $this->createUser($ownId);
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Member Successfully Registered"]);
      }else{
      	return $mem->errors;
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Member Not Registered"]);
      }
    }

  public function update(Request $request,$id){
     $mem = Member::find($id);
     $parentId=$request->input("parent_id",null);
     $branchValue=$request->input("branch",null);
     if($mem->validate($request->all())){
        $mem->fill($request->all());
         $mem->side=$branchValue;
            if($parentId==null){
              $mem->parent_id=0;
            }
            if(($totalBranch=Member::where('parent_id','=',$parentId)->count())>=2){
            return json_encode(['status'=>0,'title'=>"error",'text'=>"Parent Already Have Left And Right Member."]);
            }else if($totalBranch>0){
             $branchCheck=Member::where('parent_id','=',$parentId)->select("branch")->get(0);
            $branchVal=substr($branchCheck->first()->branch,-1);
             if($branchVal==$branchValue){

             	if($branchValue==1){
             		$bName="Left";
             	}else{$bName="Right";}

             	return json_encode(['status'=>0,'title'=>"error",'text'=>$bName." Branch Already Exist."]);
             }
            }

           $mem->save();

           $ownId=$mem->id;
           $this->createPath($parentId,$ownId);
           $this->createBranch($parentId,$branchValue,$ownId);
           $this->createUser($ownId);
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Member Successfully Registered"]);
      }else{
      	return $mem->errors;
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Member Not Registered"]);
      }
  }

    public function createUser($ownId){
        $mem = new Users();
                $mem->member_id=$ownId;
               $mem->username=$ownId;
               $mem->password="12345";
               $mem->role="user";
                $mem->save();
          return true;
    }

public function createPath($parentId,$ownId){
  $mem=Member::find($ownId);
if($parentId==null){
           $mem->path=$ownId;
           }else{
           $mem->path=$this->parentPath($parentId)."-".$ownId;
         }
         $mem->save();
      return true;
}


  public function parentPath($pId){
      $p=Member::find($pId,["path"]);
      return $p->path;
   }


public function createBranch($parentId,$branchValue,$ownId){
  $mem=Member::find($ownId);
  $b=$this->parentBranch($parentId);
  if($b==null){
    $mem->side=0;
  	$mem->branch=$branchValue;
  }else{
  $mem->branch=$b."-".$branchValue;
         }
         $mem->save();
      return true;
}


  public function parentBranch($pId){
      $p=Member::find($pId,["branch"]);
      return $p->branch;
   }


    }
