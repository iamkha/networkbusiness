<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Level;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class LevelController extends Controller {

    public function index() {


      return view('level.index');

    }
      public function creates(Request $request){
        $coun = new Level();
        if($coun->validate($request->all())){
            $coun->fill($request->all());
            $coun->save();

            return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
        }else{
            return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
        }
    }

    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
    if($search==null){
       return $lvl = DB::table('Level')->paginate($entry,['*'],'page', $page );

      // $table=$this->getData($countries);
     }
     else{

       $lvl=DB::table('Level')->where('level', 'LIKE', "%$search%")->orwhere('incentive','LIKE',"%$search%")->paginate($entry,['*'],'page', $page );
       return $lvl;
     }
}

public function edits($id){
    $coun = Level::find($id,['id','rank','balance','level','incentive']);
    return $coun;

  }
  public function updates(Request $request,$id){
     $coun = Level::find($id);
    if($coun->validate($request->all())){
        $coun->fill($request->all());
        $coun->save();
        return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
    }else{
        return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update"]);
    }
  }
  public function deletes($id){
      $coun = Level::find($id);
      try{
      $coun->delete();
      return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
    }catch(\Exception $e){
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
    }
  }

}
