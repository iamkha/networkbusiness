<?php
namespace App\Http\Controllers;
use App\Member;

use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;



class MemberlistController extends Controller {

    public function index() {
        return view('memberlist.index');
    }

    public function viewuserlist(Request $request) {

    $entry=$request->input("entry");
   $search=$request->input("search",null);
    $page=$request->input("page",null);
   // return [$pgno,$srch];
     if($page==null){
        $page=1;
      }
  if($search==null){
     return $memlist = DB::table('member')->orderBy('id',"desc")->paginate($entry,['*'],'page', $page );

    // $table=$this->getData($countries);
   }
   else{

    return $memlist=DB::table('member')->where(function($q) use ($search){
      $q->where('fullname', 'LIKE', "%$search%")
      ->orwhere('email','LIKE',"%$search%");})
      ->orderBy("id","desc")->paginate($entry,['*'],'page', $page );

   }

  }
}
