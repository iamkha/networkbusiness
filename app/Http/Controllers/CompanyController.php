<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyConfig;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class CompanyController extends Controller {

    public function index() {


      return view('company.index');

    }
  //   public function create(Request $request){
  //     $comp=new CompanyConfig();
  //     if($comp->validate($request->all())){
  //       $comp->fill($request->all());
  //       $comp->save();
  //       return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
  //     }else{
  //       return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
  //     }
  // }
  public function createcompanyname(Request $request){
    $crate=$request->input("company_name");

    DB::table('company_config')
        ->where('id', 1)
         ->limit(1)
        ->update(array('company_name' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"Error",'text'=>"Error to save data"]);

    }

  }
  public function createslogan(Request $request){
    $crate=$request->input("slogan");
    DB::table('company_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('slogan' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }

  public function createaddress(Request $request){
    $crate=$request->input("address");

    DB::table('company_config')
        ->where('id', 1)
        // ->limit(1)
        ->update(array('address' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
}
