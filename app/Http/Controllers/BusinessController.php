<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessConfig;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class BusinessController extends Controller {

    public function index() {


      return view('business.index');

    }
  //   public function create(Request $request){
  //     $coun = new BusinessConfig();
  //     if($coun->validate($request->all())){
  //         $coun->fill($request->all());
  //         $coun->save();
  //
  //         return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
  //     }else{
  //         return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
  //     }
  // }
  public function createcomission(Request $request){
    $crate=$request->input("comission_rate");

    DB::table('business_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('comission_rate' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
  public function createcapping(Request $request){
    $crate=$request->input("capping_amount");
    DB::table('business_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('capping_amount' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
  public function createminpurchase(Request $request){
    $crate=$request->input("min_purchase_amt");
    DB::table('business_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('min_purchase_amt' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
  public function createpayout(Request $request){
    $crate=$request->input("payout_period");
    DB::table('business_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('payout_period' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
  public function createminpayout(Request $request){
    $crate=$request->input("min_payout_amt");
    DB::table('business_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('min_payout_amt' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
  public function createbonusrate(Request $request){
    $crate=$request->input("bonus_rate");
    DB::table('business_config')
        ->where('id', 1)
        ->limit(1)
        ->update(array('bonus_rate' => $crate));
        if(true){
      return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
    }else{
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);

    }

  }
}
