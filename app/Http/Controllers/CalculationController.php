<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Rembalance;
use DB;
use App\Member;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class CalculationController extends Controller {


public function index(){
$mem=DB::table("member")->first();
$path=$mem->id;
$this->calculate($id);


}


public function calculate($id){
$mem=DB::table("member")->where("id","=",$id);
$path=$mem->path;
$branch=$mem->branch;
$left=DB::table("member")->where('path',"LIKE","$path%")->where("branch","LIKE","$branch"."-1%")->pluck("id")->toArray();
$right=DB::table("member")->where('path',"LIKE","$path%")->where("branch","LIKE","$branch"."-2%")->pluck("id")->toArray();

$ownBal=intval(DB::table('transaction')->where("member_id","=",$id)->sum('amount'));
// if($ownBal>500 && $mem->color=="G"){
$leftAmt=$this->calculateBranchBalance($left);
$rightAmt=$this->calculateBranchBalance($right);
// }
return ["left"=>$leftAmt,"right"=>$rightAmt];

}

public function calculateBranchBalance($side){
  $balance=0;
foreach($side as $s){

  $tran=DB::table('transaction')->where("member_id","=",$s)->sum('amount');
  $color=DB::table('member')->where("id","=",$s)->value('color');
     if($tran>500 && $color=="G"){
    $balance+=$tran;
    $a=DB::table('rem_bal');
   $a->when($a->where('member_id','=',$s)->value("id"),function($q) use (&$balance){
     if(($l=($q->value("left_balance")))==0 && ($r=($q->value("right_balance")))==0){
       $balance+=0;
      }else{
         if($l!=0){
          $balance+=$l;


           }else{
           $balance+=$r;
              }
         }
         return $balance;
     });

   }
}
  return $balance;
}





  }
