<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Rembalance;
use DB;
use App\Member;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class TransactionController extends Controller {

    public function index() {


      return view('transaction.index',['members'=>$this->getmembers()]);

    }
    public function getmembers(){
      return Member::select('id','fullname')->get();
  }
  public function creates(Request $request){
    $id=$request->input("member_id");
    $dates=$request->input("date");
    $yr=substr($dates,0,4);
    $mnth=substr($dates,5,2);
    $day=substr($dates,8,2);
      $comp=new Transaction();
      if($comp->validate($request->all())){
        $comp->fill($request->all());
          $comp->year=$yr;
          $comp->month=$mnth;
          $comp->day=$day;
        $comp->save();
        $col=Member::find($id);
        $c=$col->color;
        if($c=="R"){
          $ownBal=intval(DB::table('transaction')->where("member_id","=",$id)->sum('amount'));
         if($ownBal>=5000){
          $col->color="G";
          $col->save();
         }

        }
        return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $a=json_encode($comp->errors);
        return json_encode(['status'=>0,'title'=>"error",'text'=>$a]);
      }
  }

  public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }  if($search==null){
      $trans = DB::table('transaction')->select(['transaction.id','transaction.date', 'transaction.amount','member.fullname as fullname'])
      ->join('member','transaction.member_id','=','member.id')
      ->Paginate($entry,['*'],'page', $page );
      return $trans;
    }
      else{
        $trans = DB::table('transaction')->select(['transaction.id','transaction.date', 'transaction.amount','member.fullname as fullname'])
        ->join('member','transaction.member_id','=','member.id')
        ->where('transaction.amount', 'LIKE', "%$search%")

        ->orwhere('member.fullname','LIKE',"%$search%")
        ->Paginate($entry,['*'],'page', $page );
        return $trans;
      }

  }
  public function edits($id){
      $coun = Transaction::find($id,['id','member_id','amount','date']);
      return $coun;

    }
    public function updates(Request $request,$id){
      $uId=$request->input('member_id');
       $coun = Transaction::find($id);
       $dates=$request->input("date");
       $yr=substr($dates,0,4);
       $month=substr($dates,5,2);
       $day=substr($dates,8,2);
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->year=$yr;
          $coun->month=$month;
          $coun->day=$day;
          $coun->save();
         $col=Member::find($uId);
         $ownBal=intval(DB::table('transaction')->where("member_id","=",$uId)->sum('amount'));
         // dd($ownBal);
         if($ownBal>=5000){
          $col->color="G";
          $col->save();
         }else{
            $col->color="R";
          $col->save();
          
         }
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update"]);
      }
    }
    public function deletes($id){
      $coun = Transaction::find($id);
      try{
      $coun->delete();
      $uId=$coun->member_id;
      $col=Member::find($uId);
         $ownBal=intval(DB::table('transaction')->where("member_id","=",$uId)->sum('amount'));
         // dd($ownBal);
         if($ownBal>=5000){
          $col->color="G";
          $col->save();
         }else{
            $col->color="R";
          $col->save();
          
         }
      return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
    }catch(\Exception $e){
      return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
    }
  }

public function showBalance($id){

$mem=Member::find($id);
$name=$mem->fullname;
$path=$mem->path;
$branch=$mem->branch;
$left=DB::table("member")->where('path',"LIKE","$path%")->where("branch","LIKE","$branch"."-1%")->pluck("id")->toArray();
$right=DB::table("member")->where('path',"LIKE","$path%")->where("branch","LIKE","$branch"."-2%")->pluck("id")->toArray();

$ownBal=intval(DB::table('transaction')->where("member_id","=",$id)->sum('amount'));

$leftAmt=$this->calculateBranchBalance($left);
$rightAmt=$this->calculateBranchBalance($right);

return['own'=>$ownBal,'left'=>$leftAmt,'right'=>$rightAmt,'names'=>$name,];


}

public function calculateBranchBalance($side){
  $balance=0;
foreach($side as $s){

  $tran=DB::table('transaction')->where("member_id","=",$s)->sum('amount');

    $balance+=$tran;
   }
  return $balance;
}

public function remainingBalance(){

$a=DB::table('rem_bal');
   $a->when($a->where('member_id','=',$s)->value("id"),function($q) use (&$balance){
     if(($l=($q->value("left_balance")))==0 && ($r=($q->value("right_balance")))==0){
       $balance+=0;
      }else{
         if($l!=0){
          $balance+=$l;


           }else{
           $balance+=$r;
              }
         }
         return $balance;
     });

}



  }
