<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends BaseModel
{
    protected $table='users';
    protected $fillable=['username','password','role','member_id'];
    protected $rules=[
      'username'=>'string|required',
      'password'=>'string|required',
      'role' => 'string',
      'member_id'=>'integer',



    ];

}
