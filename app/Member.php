<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends BaseModel
{
    protected $table='member';
    protected $fillable=['fullname','dob','sex','fathers_name','marital_status','spouse_name','perm_address','temp_address','pan_no','introducer_name','introducer_id','spiller_name','spiller_id','parent_id','stockist_name','bank_ac_no','bank_name','mobile','path','branch','side','color'];
    protected $rules=[
      'fullname'=>'string|required',
      'dob'=>'nullable|string',
      'sex'=>'nullable|string',
      'fathers_name'=>'nullable|string',
      'marital_status'=>'nullable|string',
      'spouse_name'=>'nullable|string',
      'perm_address'=>'nullable|string',
      'temp_address'=>'nullable|string',
      'pan_no'=>'nullable|string',
      'introducer_name'=>'nullable|string',
      'introducer_id'=>'nullable|integer',
      'spiller_name'=>'nullable|string',
      'spiller_id'=>'integer|required',
      'parent_id'=>'integer|required',
      'stockist_name'=>'nullable|string',
      'bank_ac_no'=>'nullable|string',
      'bank_name'=>'nullable|string',
      'mobile'=>'nullable|integer',
      'color'=>'string|required',
      'path'=>'string',
       'branch'=>'string|required',
        'side'=>'integer'
    ];

}
