<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends BaseModel
{
    protected $table='level';
    protected $fillable=['level','balance','rank','incentive'];
    protected $rules=[
      'level'=>'string|required',
       'balance'=>'integer',
       'rank'=>'integer',
       'incentive'=>'string',


    ];

}
