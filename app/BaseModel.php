<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BaseModel extends Model
{
    protected $rules =[];
    public $errors=[];
    public $timestamps = false;
    
    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors();
            return false;
        }

        // validation pass
        return true;
    }
    
    //this function changes nepali number to english and english no to nepali
    public static function toggleNumber($n) {
        $langMap = ["०", "१", "२", "३", "४", "५", "६", "७", "८", "९"];
        $num = '';
        $nArray = preg_split('/(.{0})/us', $n, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
        foreach ($nArray as $sn) {
            if (isset($langMap[$sn])) {
                $num .=$langMap[$sn];
            } elseif (in_array($sn, $langMap)) {
                $num .= array_search($sn, $langMap);
            }
            else{
                $num .=$sn;
            }
        }
        return $num;
    }
}
