<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends BaseModel
{
    protected $table='transaction';
    protected $fillable=['member_id','amount','date'];
    protected $rules=[
      'member_id'=>'integer|required',
      'amount'=>'integer|required',
      'date' => 'required|date',
      'year'=>'nullable|integer',
      'month'=>'nullable|integer',
      'day'=>'nullable|integer',



    ];

}
