<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessConfig extends BaseModel
{
    protected $table='business_config';
    protected $fillable=['comission_rate','capping_amount','min_purchase_amt','payout_period','min_payout_amt','bonus_rate'];
    protected $rules=[
      'comission_rate'=>'integer',
      'capping_amount'=>'integer',
      'min_purchase_amt'=>'integer',
      'payout_period'=>'integer',
      'min_payout_amt'=>'integer',
      'bonus_rate'=>'integer',

    ];

}
