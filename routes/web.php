<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {

    return view('login.login');
   });

Route::get('/calculate',"CalculationController@index");

Route::group(['prefix'=>"register"],function(){
        Route::post('create','RegisterController@create');
          Route::get('edits/{id}','RegisterController@edits');
            Route::post('update/{id}','RegisterController@update');


});

Route::group(['prefix'=>"business"],function(){
  Route::get('/','BusinessController@index');
  Route::post('create','BusinessController@create');
  Route::post('createcomission','BusinessController@createcomission');
  Route::post('createcapping','BusinessController@createcapping');
  Route::post('createminpurchase','BusinessController@createminpurchase');
  Route::post('createpayout','BusinessController@createpayout');
  Route::post('createminpayout','BusinessController@createminpayout');
  Route::post('createbonusrate','BusinessController@createbonusrate');

});
 Route::group(['prefix'=>"company"],function(){
   Route::get('/','CompanyController@index');
   Route::post('create','CompanyController@create');
   Route::post('createcompanyname','CompanyController@createcompanyname');
   Route::post('createaddress','CompanyController@createaddress');
   Route::post('createslogan','CompanyController@createslogan');
});
Route::group(['prefix'=>"level"],function(){
  Route::get('/','LevelController@index');
  Route::post('creates','LevelController@creates');
  Route::get('lists','LevelController@lists');
  Route::get('edits/{id}','LevelController@edits');
Route::post('updates/{id}','LevelController@updates');
Route::get('deletes/{id}','LevelController@deletes');

});
 Route::group(['prefix'=>"tree"],function(){
   Route::get('/','TreeController@index');
      Route::get('construct/{id}','TreeController@aa');
 });
 Route::group(['prefix'=>"transaction"],function(){
   Route::get('/','TransactionController@index');
    Route::post('creates','TransactionController@creates');
    Route::get('lists','TransactionController@lists');
    Route::get('edits/{id}','TransactionController@edits');
  Route::post('updates/{id}','TransactionController@updates');
  Route::get('deletes/{id}','TransactionController@deletes');
  Route::get('showbal/{id}','TransactionController@showBalance');

 });

 Route::group(['prefix'=>"memberlist"],function(){
   Route::get('/','MemberlistController@index');
   Route::get('viewuserlist','MemberlistController@viewuserlist');

 });

Route::get('/logout', function () {
      Session::flush();
    return redirect("/");
});


Route::group(['prefix' =>'login'], function () {

Route::post('/','LoginController@login');
Route::post('changepass','LoginController@changePassword');
});
