<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Level
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">

                    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)">

                    <input type="hidden" name="id" id="id" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Level</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                          <?php

                         if(!empty(old())){

                             $req=old();

                              }

                          ?>
                          <div class="form-group">
                              <label for="code" class="col-sm-3 control-label">Rank</label>

                              <div class="col-sm-9">
                                  <input class="form-control" id="rank" placeholder="Rank" type="text" name="rank" value="">
                              </div>
                          </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Level</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="level" placeholder="Level Name" type="text" name="level" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Required Balance</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="balance" placeholder="Required Balance" type="text" name="balance" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Incentives</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="incentive" placeholder="incentives" type="text" name="incentive" value="">
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-6">
                <div class="box col-md-4">
                    <div class="box-header with-border">
                        <h3 class="box-title">Level List</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div id="table" class="box-body">
                        <form id="tableform" >
                        <b>SHOW</b><select id="selectentry" onchange="table(event)">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                            <b>ENTRIES</b>
                          </form>
                            <div style="float:right">
                      <form id="srch" name="srch" onsubmit="searchClicked(event)" >
                    <input  id="searchfill" placeholder="  search here" type="text" name="search">
                    <button type="submit"  id="searchbtn" name="submit">Search</button>
                  </form>
                </div>

                        <div id="showtable" class="box-body">
                            <table id="level-table" class="table table-striped table-bordered">
                              <tr><th>ID</th><th>Rank</th><th>Level</th><th>Needed Balance</th><th>Incentives</th><th>ACTIONS</th></tr>

                                </table>
                        </div>
                      <div id="pagg">

                        <ul class="pagination pagination-sm">

                        </ul>

                          </div>




                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
var hostName=window.location.hostname;


function table(){

    var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseurl+"/level/lists/?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTable(response);

    },
    fail:function(){
        alert("failed");
    }
});


 $("body").on("click",".pagination li a",function(e,records=0){

  e.preventDefault();
  var entry=$("#selectentry").val();
  if (records!=0){
    records=records-1;
  var entry=$("#selectentry").val();
var lastIndex=$("body #pagg ul li").length-2;
 if(records%entry!=0){
  page=lastIndex;
 }else{
   page=lastIndex+1;
 }
}
else{
  var page=e.target.text;;
var index= $("body #pagg ul li.active").text();
 var lastIndex=$("body #pagg ul li").length-2;

if(page==="Prev"){

  if(index=="1"){page=index;}
  else{
  index--;

  page=index;
}}
if(page==="Next"){
  if(index==lastIndex){
    page=lastIndex;
  }
else{
  index++;

  page=index;
}}
}


$.ajax({

    method:'get',
    url:baseurl+"/level/lists/?entry="+entry+"&page="+page+"&search="+$("#searchfill").val(),
    success:function(response){
        createTable(response);



    },
    fail:function(){
        alert("failed");
    }
});

  });

}

function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseurl+"/level/lists/?entry="+entry+"&search="+search,
    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});
}



function createTable(resp){
    var t=document.getElementById("level-table");
   $("body #level-table").find("tr:gt(0)").remove();
    //}
    var rowCount=1;
    var data=resp.data;
    for(var i in data){
    var row=t.insertRow(rowCount);

    row.insertCell(0).innerHTML=data[i].id;
      row.insertCell(1).innerHTML=data[i].rank;
    row.insertCell(2).innerHTML=data[i].level;
    row.insertCell(3).innerHTML=data[i].balance;
    row.insertCell(4).innerHTML=data[i].incentive;

    row.insertCell(5).innerHTML="<a href='javascript:void(0)' onclick='edit("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='delt("+data[i].id+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
        rowCount++;

   }
 callForPagination(resp);
   return true;
}

   function formSubmit(e){
    e.preventDefault();

         if( $('#id').val()==""){
           var url=baseurl+"/level/creates";
            }
           else{
            var url=baseurl+"/level/updates/"+ $('#id').val();
            }

       $.ajax({
           method:"POST",
           url:url,
           data:$("#form").serialize(),
           success:function(resp){
            var a =JSON.parse(resp);
             $('#level').val("");
             $('#id').val("");
             $('#balance').val("");
             $('#rank').val("");
             $('#incentive').val("");

            $("#submit").text("create");
            $('#id').val("");
             if(a.records){
                 var records=a.records;
                     a.records.delete;
                     toast(a);
                        $("body .pagination li a").trigger("click",records);

               }else{
                     toast(a);
                     table();
               }


        },
          fail:function(){

          alert("failed");
     }
      });
}

function edit(id){

    $("#submit").text("update");
     $.ajax({
     method:'get',
    url:baseurl+"/level/edits/"+id,
     success:function(resp){
             $('#id').val(resp.id);
             $('#level').val(resp.level);
             $('#balance').val(resp.balance);
             $('#rank').val(resp.rank);
             $('#incentive').val(resp.incentive);

         },
        fail:function(){

         }
     });

   }


function delt(id){
    var conf=confirm("Are you sure you want to delete?");
   if(conf){
    $.ajax({
        method:'get',
        url:baseurl+"/level/deletes/"+id,
        success:function(resp){
             var a =JSON.parse(resp);
             toast(a);
             table();

        },

        fail:function(){
            alert("Fail");

        }
    });
}
}
function resetForm(){
  $('#id').val('');
  $('#submit').text('Create');
}

</script>
<?php include(resource_path().'/views/sections/footer.php');
