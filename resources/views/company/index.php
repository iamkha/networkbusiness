<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-7">

                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Company configuration</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                          <form class="form-horizontal" id="companyform" onsubmit="companyname(event)" method="post" action="">
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="company_name" class="col-sm-3 control-label">Company Name</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="company_name" placeholder="company name" type="text" name="company_name" value="">
                                </div>
                                <button type="submit" id="submit" class="btn btn-success">Save</button>

                            </div>
                          </form>
                          <form class="form-horizontal" id="addressform" onsubmit="createaddress(event)" method="post" action="">
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="address"  placeholder="address" type="text" name="address" value="">
                                </div>
                                <button type="submit" id="submit" class="btn btn-success">Save</button>

                            </div>
                          </form>
                          <form class="form-horizontal" id="sloganform" onsubmit="createslogan(event)" method="post" action="">
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="slogan" class="col-sm-3 control-label">Slogan</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="slogan" placeholder="slogan" type="text" name="slogan" value="">
                                </div>
                                <button type="submit" id="submit" class="btn btn-success">Save</button>

                            </div>
                          </form>


                        </div>

                    </div>

            </div>

            <div class="col-md-5">
                <div class="box col-md-4">
                    <div class="box-header with-border">
                        <h3 class="box-title"> List items</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>


                    </form>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
// function formSubmit(e){
//   e.preventDefault();
//   $.ajax({
//     method:"POST",
//     url:"http://localhost/nb/company/create",
//     data:$('#form').serialize(),
//     success:function(resp){
//         $('#company_name').val("");
//           $('#address').val("");
//             $('#slogan').val("");
//       var a=JSON.parse(resp);
//       toast(a);
//     },
//     fail:function(resp){
//       alert("failed");
//     }
//   });
// }
function companyname(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/company/createcompanyname",
    data:$('#companyform').serialize(),
    success:function(resp){
        $('#company_name').val("");

      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });
}
function createslogan(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/company/createslogan",
    data:$('#sloganform').serialize(),
    success:function(resp){

        $('#slogan').val("");

      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });
}
function createaddress(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/company/createaddress",
    data:$('#addressform').serialize(),
    success:function(resp){
        $('#address').val("");

      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });
}
</script>
<?php include(resource_path().'/views/sections/footer.php');
