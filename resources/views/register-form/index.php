
<div  id="reg-form" class="content-wrapper" style="height: 1000px;display:none;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
         New Membership Registration Form.
        </h1>
    </section>

    <!-- Main content -->
        <!-- Default box -->
        <div class="col-md-8">
            <form class="form-horizontal" id="register-form" onsubmit="registerMember(event)">
                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                 <input type="hidden" name="id" id="id" value="">

                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Member</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Full Name:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="fullname" placeholder="Full Name" type="text" name="fullname" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Date Of Birth:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="dob" placeholder="YYYY/MM/DD" type="text" name="dob">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Sex:*</label>

                                <div class="col-sm-5">
                                    <input type="radio" name="sex" value="Male">Male
                                    <input type="radio" name="sex" value="Female">Female
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Father Name:</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="father-name" placeholder="Father Name" type="text" name="fathers_name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Marital Status:*</label>

                                <div class="col-sm-5">
                                    <input type="radio" onclick={$("#spouse").show();}name="marital_status" value="Married">Married
                                    <input type="radio" onclick={$("#spouse").hide();} name="marital_status" value="Unmarried">Unmarried
                                </div>
                            </div>
                            <div id="spouse" class="form-group" style="display:none">
                                <label for="code" class="col-sm-3 control-label">Spouse Name:</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="spouse-name" placeholder="Spouse Name" type="text" name="spouse_name" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Permanent Address:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="perm_address" placeholder="Permanent Address" type="text" name="perm_address" >
                                </div>
                            </div>
                           <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Temporary Address:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="temp_address" placeholder="Temporary Address" type="text" name="temp_address" >
                                </div><br>
                            </div>
                                <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Pan No:</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="pan_no" placeholder="Pan No" type="text" name="pan_no" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Introducer Name:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="introducer_name" placeholder="Introducer Name" type="text" name="introducer_name" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Introducer Id:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="introducer_id" placeholder="Introducer Id" type="text" name="introducer_id" >
                                </div>
                            </div>

                                <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Spiller Name:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="spiller_name" placeholder="Spiller Name" type="text" name="spiller_name" >
                                </div><br>
                            </div>
                            <input class="form-control" id="parent_id"  type="hidden" name="parent_id" >

                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Spiller Id:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="spiller_id" placeholder="spiller Id" type="text" name="spiller_id" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Select Branch:*</label>

                                <div class="col-sm-5">
                                    <input type="radio" name="branch" value="1">Left(<<)
                                    <input type="radio" name="branch" value="2">Right(>>)
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="type" class="col-sm-3 control-label">Distributer Type:*</label>

                            <div class="col-sm-5">
                              <select class="form-control" style="height=20px;" id="color" name="color" >
                                       <option id="select" value="0">Select Type</option>
                                       <option value="R">Free</option>
                                       <option value="G">Paid</option>
                                     </select>
                            </div>
                        </div>

                                <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Stockist Name:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="stokist_name" placeholder="Stokist Name" type="text" name="stockist_name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Bank Name:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="bank_name" placeholder="Bank Name" type="text" name="bank_name" >
                                </div>
                            </div>

                           <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Bank Account No:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="bank_ac_no" placeholder="Bank Account No" type="text" name="bank_ac_no" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Mobile NO:*</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="mobile" placeholder="Mobile No" type="text" name="mobile" >
                                </div>
                            </div


                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit"  class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div>
                        </form>
                </div>
                     </div>
           </div>
                </div>
