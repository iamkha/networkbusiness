<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Business
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-7">

                    <!-- <form class="form-horizontal" id="form"  method="post" action=""> -->


                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create business configuration</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                          <form class="form-horizontal" id="comissionForm" onsubmit="createComission(event)" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="comission_rate" class="col-sm-3 control-label">Comisssion Rate</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="comission_rate" placeholder="comission rate" type="text" name="comission_rate" value="">

                                </div>
                                <button type="submit" id="submit"  class="btn btn-success">Save</button>

                            </div>
                          </form>


                          <form class="form-horizontal" id="cappingForm" onsubmit="createCapping(event)" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="capping_amount" class="col-sm-3 control-label">Capping Amount</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="capping_amount" placeholder="capping amount" type="text" name="capping_amount" value="">
                                </div>
                                <button type="submit" id="submit"  class="btn btn-success">Save</button>

                            </div>
                          </form>


                          <form class="form-horizontal" id="purchaseform" onsubmit="createMinpurchase(event)" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="min_purchase" class="col-sm-3 control-label">Minimum Purchase Amount</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="min_purchase_amt" placeholder="minimum purchase amount" type="text" name="min_purchase_amt" value="">
                                </div>
                                <button type="submit" id="submit"  class="btn btn-success">Save</button>

                            </div>
                          </form>
                            <form class="form-horizontal" id="payoutform" onsubmit="createPayout(event)" method="post">
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="payout" class="col-sm-3 control-label">Payout Duration</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="payout_period" placeholder="payout period days" type="text" name="payout_period" value="">
                                </div>
                                <button type="submit" id="submit"  class="btn btn-success">Save</button>
                            </div>

                          </form>
                          <form class="form-horizontal" id="minpayoutform" onsubmit="createMinpayout(event)" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="min_payout" class="col-sm-3 control-label">Minimum Payout Amount</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="min_payout_amt" placeholder="minimum payout amount" type="text" name="min_payout_amt" value="">
                                </div>
                                    <button type="submit" id="submit"  class="btn btn-success">Save</button>
                            </div>
                          </form>


                          <form class="form-horizontal" id="bonusform" onsubmit="createBonusrate(event)" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label for="bonus" class="col-sm-3 control-label">Bonus Rate</label>

                                <div class="col-sm-5">
                                    <input class="form-control" id="bonus_rate" placeholder="rate of bonus" type="text" name="bonus_rate" value="">
                                </div>
                                <button type="submit" id="submit"  class="btn btn-success">Save</button>

                            </div>


                          </form>

                        </div>
                        <!-- <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div> -->
                    </div>
                </form>
            </div>

            <div class="col-md-5">
                <div class="box col-md-4">
                    <div class="box-header with-border">
                        <h3 class="box-title"> List items</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>


                    </form>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
// var hostName=window.location.hostname;
// function formSubmit(e){
//   e.preventDefault();
//   $.ajax({
//     method:"POST",
//     url:"http://localhost/nb/business/create",
//     data:$('#form').serialize(),
//     success:function(resp){
//       $('#comission_rate').val("");
//       $('#capping_amount').val("");
//       $('#min_purchase_amt').val("");
//       $('#payout_period').val("");
//       $('#min_payout_amt').val("");
//       $('#reward_period').val("");
//       $('#bonus_rate').val("");
//       var a=JSON.parse(resp);
//       toast(a);
//     },
//     fail:function(resp){
//       alert("failed");
//     }
//   });
// }
function createComission(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/business/createcomission",
    data:$('#comissionForm').serialize(),
    success:function(resp){
      $('#comission_rate').val("");
      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });

}
function createCapping(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/business/createcapping",
    data:$('#cappingForm').serialize(),
    success:function(resp){
      $('#capping_amount').val("");
      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });

}
function createMinpurchase(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/business/createminpurchase",
    data:$('#purchaseform').serialize(),
    success:function(resp){
      $('#min_purchase_amt').val("");
      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });

}
function createPayout(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/business/createpayout",
    data:$('#payoutform').serialize(),
    success:function(resp){
      $('#payout_period').val("");
      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });

}
function createMinpayout(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/business/createminpayout",
    data:$('#minpayoutform').serialize(),
    success:function(resp){
      $('#min_payout_amt').val("");
      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });

}
function createBonusrate(e){
  e.preventDefault();
  $.ajax({
    method:"POST",
    url:"http://localhost/nb/business/createbonusrate",
    data:$('#bonusform').serialize(),
    success:function(resp){
      $('#bonus_rate').val("");
      var a=JSON.parse(resp);
      toast(a);
    },
    fail:function(resp){
      alert("failed");
    }
  });

}
</script>
<?php include(resource_path().'/views/sections/footer.php');
