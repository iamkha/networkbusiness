<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<?php include(resource_path().'/views/register-form/index.php');?>
<!-- Content Wrapper. Contains page content -->
<div id="mem-list" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Member Lists
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->

        <div class="row">
            <div class="col-md-9">

                    <div class="box col-md-6">
                        <div class="box-header with-border">
                            <h3 class="box-title">Member List:</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                  <div class="box-body">
                          <div id="table" class="box-body">
                        <form id="tableform" >
                        <b>SHOW</b><select id="selectentry" onchange="table(event)">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                            <b>ENTRIES</b>
                          </form>
                            <div style="float:right">
                      <form id="srch" name="srch" onsubmit="searchClicked(event)" >
                    <input  id="searchfill" placeholder="  search here" type="text" name="search">
                    <button type="submit"  id="searchbtn" name="submit">Search</button>
                  </form>
                </div>

                        <div id="showtable" class="box-body">
                          <table id="memberlist-table" class="table table-striped table-bordered">
                              <tr><th> ID</th><th>Fullname</th><th>Actions</th></tr>

                                </table>


                        </div>
                      <div id="pagg">

                        <ul class="pagination pagination-sm">

                        </ul>

                          </div>




                    </div>


                        </div>

                    </div>
                </form>
            </div>

          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">

function table(){

var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseurl+"/memberlist/viewuserlist/?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTableFormemberlist(response);

    },
    fail:function(){
        alert("failed");
    }
});


 $("body").on("click",".pagination li a",function(e,records=0){

  e.preventDefault();
  var entry=$("#selectentry").val();
  if (records!=0){
    records=records-1;
  var entry=$("#selectentry").val();
var lastIndex=$("body #pagg ul li").length-2;
 if(records%entry!=0){
  page=lastIndex;
 }else{
   page=lastIndex+1;
 }
}
else{
  var page=e.target.text;;
var index= $("body #pagg ul li.active").text();
 var lastIndex=$("body #pagg ul li").length-2;

if(page==="Prev"){

  if(index=="1"){page=index;}
  else{
  index--;

  page=index;
}}
if(page==="Next"){
  if(index==lastIndex){
    page=lastIndex;
  }
else{
  index++;

  page=index;
}}
}


$.ajax({

    method:'get',
    url:baseurl+"/memberlist/viewuserlist/?entry="+entry+"&page="+page+"&search="+$("#searchfill").val(),
    success:function(response){
        createTableFormemberlist(response);



    },
    fail:function(){
        alert("failed");
    }
});

  });

}

function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseurl+"/memberlist/viewuserlist/?entry="+entry+"&search="+search,
    success:function(response){
      createTableFormemberlist(response);
    },
    fail:function(){
        alert("failed");
    }
});
}

function createTableFormemberlist(resp){
    var t=document.getElementById("memberlist-table");

   $("body #memberlist-table").find("tr:gt(0)").remove();

    var rowCount=1;
    var data=resp.data;

    for(var i in data){


    var row=t.insertRow(rowCount);
    row.insertCell(0).innerHTML=data[i].id;
    row.insertCell(1).innerHTML=data[i].fullname;

    row.insertCell(2).innerHTML="<a href='javascript:void(0)' onclick='edit("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>";

        rowCount++;

   }
 callForPagination(resp);
   return true;
}

function edit(id){
  $.ajax({
  method:'get',
 url:baseurl+"/register/edits/"+id,
  success:function(resp){
    // alert(resp.spiller_id);
    $("#reg-form").show();
    $("#mem-list").hide();
    $("#id").val(resp.id);
    $("#parent_id").val(resp.parent_id);
    $("#fullname").val(resp.fullname);
    $("#dob").val(resp.dob);
    $("#fathers_name").val(resp.fathers_name);
    $('#sex').val(resp.sex);
    $('#marital_status').val(resp.marital_status);
    $('#spouse_name').val(resp.spouse_name);
    $('#perm_address').val(resp.perm_address);
    $('#temp_address').val(resp.temp_address);
    $('#pan_no').val(resp.pan_no);
    $('#introducer_id').val(resp.introducer_id);
    $('#introducer_name').val(resp.introducer_name);
    $('#spiller_id').val(resp.spiller_id);
    $("#spiller_name").val(resp.spiller_name);
    //  $('#color').find(":selected").text(resp.color);
    // $('#color').val(resp.color);
    $('#stockist_name').val(resp.stockist_name);
    $('#bank_name').val(resp.bank_name);
    $('#bank_ac_no').val(resp.bank_ac_no);
    $('#mobile').val(resp.mobile);

       var a =JSON.parse(resp);
      //  console.log(a);s


             },
     fail:function(){

      }
  });

}



    function registerMember(e){
     e.preventDefault();
     if( $('#id').val()==""){
       var url="http://localhost/nb/register/create";
        }
       else{
        var url="http://localhost/nb/register/update/"+$("#id").val();
        }
      $.ajax({
          method:"post",
          url:url,
          data:$("#register-form").serialize(),
     success:function(response){
    var res=JSON.parse(response);
    toast(res);
    if(res.status==1){
      $("#reg-form").hide();
    $("#mem-list").show();

    }

     },
     fail:function(){

     }

      });



    }



</script>
<?php include(resource_path().'/views/sections/footer.php');
