<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>NB | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo asset('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo asset('css/AdminLTE.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo asset('assets/plugins/iCheck/square/blue.css');?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <?php //<!-- Google Font -->
  //<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  ?>
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <?php if(Session::has('message')):?>

<div class="alert alert-success"><h3>Successfully Registered</h3><?php echo Session::get('message'); ?></div>
<?php endif;?>
<?php if(Session::has('msgerror')):?>

<div class="alert alert-error"><h3>Login Failed.</h3><?php echo Session::get('msgerror'); ?></div>
<?php endif;?>

  <div class="login-logo">
    <a href="../../index2.html"><b>NB</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form method="post" action="<?php echo url('/login');?>" >
      <div class="form-group has-feedback">
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input id="email" type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
       <div id="error"  style="text-align:center;color:red;display:none;font-family: "Times New Roman", Georgia, Serif;font-size=20px;"><i class="fa fa-fw fa-minus-square"></i></div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button  type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          <a href="<?php echo url('/signup');?>" class="btn btn-primary btn-block btn-flat">Sign Up</a>

        </div>
        <div class="col-xs-4" style="float:left">

        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>

<!-- jQuery 2.2.3 -->
<script src="<?php echo asset('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo asset('assets/bootstrap/js/bootstrap.min.js');?>"></script>

<!-- iCheck -->
<script src="<?php echo asset('assets/plugins/iCheck/icheck.min.js');?>"></script>
<script>
  $(function(){
    <?php
    if(Session::has('error')){?>
       var emsg='<?php echo Session::get('error');?>';
       $("#error").text(emsg);
       $("#error").show();

   <?php } ?>





  });


$("#email,#password").on("focus",function(){
  $("#error").hide();
})




</script>
</body>
</html>
<?php include_once(resource_path().'/views/sections/loginfooter.php');
