<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tree Structure
        <small>it all starts here</small>
      </h1>
    </section>

    <style>
  .glyphicon {
    font-size: 25px;
  }
    </style>
        <div class="box" style="overflow:scroll;height:100%;width:100%;overflow:auto">
          <div class="col-md-6" style="margin-left: 568px;">
               <table id="transaction-table" class="table table-striped table-bordered">
                 <tr><th>Member</th><th>Purchase</th><th>Left Sale</th><th>Right Sale</th></tr>

                   </table>
           </div>

        	 <div class="box-header with-border">

            <div class="tree">
	<ul id="tree-struct">

	</ul>
  
</div>






           </div>

        	 </div>
           


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  function treeConstruct(id=<?php echo session::get('id') ?>){
   var tree=$("#tree-struct");
  var tag;
  $.ajax({
    method:"get",
    url:"<?php echo url('/tree/construct/"+id+"');?>",
     success:function(res){
         tree.empty();
         if(res.color=="R"){


      	tag="<li><div><span class='glyphicon glyphicon-tree-deciduous' style='color:red'; onclick='treeConstruct("+res.id+")'></span><div ><h4><b>"+res.name+"</b></h4><p>ID:"+res.id+"</p></div>";
         }else{
       tag="<li><div><span class='glyphicon glyphicon-tree-deciduous ' style='color:green'; onclick='treeConstruct("+res.id+")'></span><div><h4><b>"+res.name+"</b></h4><p>ID:"+res.id+"</p></div>";
          
         }
         if(res.child.length>1){
         	tag+="<ul>";
         	recursive(res.child);
           tag+="</li>";
         	}else if(res.child.length=1){
             tag+="<ul>";
          recursive(res.child);
          tag+="<button  type='button' onclick='register(event,"+res.id+")' data-toggle='modal' data-target='#modal-default' class='btn btn-xs btn-primary'><i class='fa fa-fw fa-plus'></i></button>";
         tag+="</li>";
           }else{
             tag+="<button  type='button' onclick='register(event,"+res.id+")' data-toggle='modal' data-target='#modal-default' class='btn btn-xs btn-primary'><i class='fa fa-fw fa-plus'></i></button>";
  tag+="</li>";

           }


        tree.append(tag);
        transactionShow(id);
     },
     fail:function(){

     }
  });


  function recursive(all){
  	var len=all.length;
   for(var j in all){
              if(all[j].color=="R"){ 
                 tag+="<li><div><span class='glyphicon glyphicon-tree-deciduous ' style='color:red'; onclick='treeConstruct("+all[j].id+")'></span><div><h4><b>"+all[j].name+"</b></h4><p>ID:"+all[j].id+"</p></div>";
         		      }else{
                  tag+="<li><div><span class='glyphicon glyphicon-tree-deciduous ' style='color:green'; onclick='treeConstruct("+all[j].id+")'></span><div><h4><b>"+all[j].name+"</b></h4><p>ID:"+all[j].id+"</p></div>";

                   
                  }
                if(all[j].child.length>1){
         	      	tag+="<ul>";

         	      	recursive(all[j].child);
         	      }else if(all[j].child.length==1){
                  tag+="<button  type='button' onclick='register(event,"+all[j].id+")' data-toggle='modal' data-target='#modal-default' class='btn btn-xs btn-primary'><i class='fa fa-fw fa-plus'></i></button>";

                  tag+="<ul>";

                  recursive(all[j].child);

                }else{
                  tag+="<button  type='button' onclick='register(event,"+all[j].id+")' data-toggle='modal' data-target='#modal-default' class='btn btn-xs btn-primary'><i class='fa fa-fw fa-plus'></i></button>";
         	      	tag+="</li>";
         	      }
         	      len--;
         	      if(len==0){
                   tag+="</ul>";
               }

         	      }
         	      return tag;
         	}

}
function transactionShow(id){
 $.ajax({
  method:"get",
  url:"<?php echo url('/transaction/showbal/"+id+"');?>",
   success:function(resp){

    createTable(resp);
},
fail:function(res){

}
});


}




  function createTable(resp){

      var t=document.getElementById("transaction-table");
     $("body #transaction-table").find("tr:gt(0)").remove();

      var rowCount=1;
      // var data=resp.data;
      // for(var i in data){
      var row=t.insertRow(rowCount);

      row.insertCell(0).innerHTML=resp.names;
      row.insertCell(1).innerHTML=resp.own;
      row.insertCell(2).innerHTML=resp.left;
      row.insertCell(3).innerHTML=resp.right;
          rowCount++;



      //  return true;
  }

  function register(e,id){
      $("#modal-heading").text("Add Distributor.");
      $("#spiller_id").val('<?php echo Session::get('id');?>');
         $("#spiller_name").val('<?php echo Session::get('fullname');?>');
      $("#spiller_id,#spiller_name").attr('readonly',true);
      if("<?php echo Session::get('role');?>"=="user"){
          $("#select-d").empty().append('<option value="R" selected readonly>Paid</option>');
      }
   $("#parent_id").val(id);
}




        function registerMember(e){
         e.preventDefault();
          $.ajax({
              method:"post",
              url:"http://localhost/nb/register/create",
              data:$("#register-form").serialize(),
         success:function(response){
        var res=JSON.parse(response);
        if(res.status==1){
        $("#fullname").val("");
      
       }
        toast(res);
        treeConstruct();
         },
         fail:function(){

         }

          });



        }


// <li>
// 			<a href="#">Parent</a>
// 			<ul>
// 				<li>
// 					<a href="#">Child</a>
// 					<ul>
// 						<li>
// 							<a href="#">Grand Child</a>
// 						</li>
// 					</ul>
// 				</li>
// 				<li>
// 					<a href="#">Child</a>
// 					<ul>
// 						<li><a href="#">Grand Child</a></li>
// 						<li>
// 							<a href="#">Grand Child</a>
// 							<ul>
// 								<li>
// 									<a href="#">Great Grand Child</a>
// 								</li>
// 								<li>
// 									<a href="#">Great Grand Child</a>
// 								</li>
// 								<li>
// 									<a href="#">Great Grand Child</a>
// 								</li>
// 							</ul>
// 						</li>
// 						<li><a href="#">Grand Child</a></li>
// 					</ul>
// 				</li>
// 			</ul>
// 		</li>



  </script>
<?php include(resource_path().'/views/sections/footer.php');
