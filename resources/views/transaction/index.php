<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">

                    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)" method="post" action="">

                        <input type="hidden" id="id" name="id" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Transaction of Member</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Member Name</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="member_id" name="member_id">
                                        <option value="">Select one..</option>
                                        <?php if(isset($members) && count($members)>0):?>
                                            <?php foreach($members as $c):?>
                                                <?php if(isset($update) && ($update->member_id == $c->id)):?>
                                                  <option value="<?php echo $c->id;?>" selected="selected"><?php echo $c->fullname;?></option>
                                                <?php else:?>
                                                <option value="<?php echo $c->id;?>"><?php echo $c->fullname;?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Amount</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="amount" placeholder="Purchase Amount" type="text" name="amount" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Purchase Date</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="date"  type="date" name="date" value="">
                                </div>
                            </div>


                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">Transaction</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
         <div id="table" class="box-body">
            <form id="tableform" >
            <b>SHOW</b><select id="selectentry" onchange="table(event)">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
                <b>ENTRIES</b>

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn" name="submit">Search</button>
      </form>
    </div>

            <div id="showtable" class="box-body">
                <table id="transaction-table" class="table table-striped table-bordered">
                  <tr><th>ID</th><th>Member Name</th><th>Purchase Amount</th><th>Date</th><th>ACTIONS</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>

              </div>
        </form>


        </div>
    </div>
</div>

        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
var hostName=window.location.hostname;

function table(){

    var entry=$("#selectentry").val();
$("#tableform").on("submit",function(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
      url:"http://localhost/nb/transaction/lists/?entry="+entry+"&search="+search,


    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});

});


$("body").on("click",".pagination li a",function(e,records=0){
  e.preventDefault();
  if (records!=0){
  var entry=$("#selectentry").val();
records=records-1;
var lastIndex=$("body #pagg ul li").length-2;
 if(records%entry!=0){
  page=lastIndex;
 }else{
   page=lastIndex+1;
 }
}
else{
  var page=e.target.text;;
var index= $("body #pagg ul li.active").text();
 var lastIndex=$("body #pagg ul li").length-2;

if(page==="Prev"){

  if(index=="1"){page=index;}
  else{
  index--;

  page=index;
}}
if(page==="Next"){
  if(index==lastIndex){
    page=lastIndex;
  }
else{
  index++;

  page=index;
}}
}  var entry=$("#selectentry").val();


  if(($("#searchfill").val())!=""){
    url="http://localhost/nb/transaction/lists/?entry="+entry+"&page="+page+"&search="+$("#searchfill").val();
  }else{
    url="http://localhost/nb/transaction/lists/?entry="+entry+"&page="+page;
  }


$.ajax({

    method:'get',
    url:url,
    success:function(response){

        createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});

  });

 if(($("#searchfill").val())!=""){
    $("#tableform").trigger("submit");
 }

var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index
}
$.ajax({

    method:'get',
      url:"http://localhost/nb/transaction/lists/?entry="+entry+"&page="+page,
    success:function(response){

        createTable(response);

           },
    fail:function(){
        alert("failed");
    }
});
}

function createTable(resp){

    var t=document.getElementById("transaction-table");
   $("body #transaction-table").find("tr:gt(0)").remove();

    var rowCount=1;
    var data=resp.data;
    for(var i in data){
    var row=t.insertRow(rowCount);

    row.insertCell(0).innerHTML=data[i].id;
    row.insertCell(1).innerHTML=data[i].fullname;
    row.insertCell(2).innerHTML=data[i].amount;
    row.insertCell(3).innerHTML=data[i].date;
    row.insertCell(4).innerHTML="<a href='javascript:void(0)' onclick='edit("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='delt("+data[i].id+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
        rowCount++;

   }
   callForPagination(resp);
     return true;
}

   function formSubmit(e){
    e.preventDefault();
    if( $('#id').val()==""){
           var url="http://localhost/nb/transaction/creates";
            }
           else{
            var url="http://localhost/nb/transaction/updates/"+ $('#id').val();
            }
       $.ajax({
           method:"POST",
           url:url,
           data:$("#form").serialize(),
           success:function(resp){
            var a =JSON.parse(resp);
            $("#member_id").val("");
             $('#amount').val("");
             $('#date').val("");


               table();
                if(a.records){
                 var records=a.records;
                   a.records.delete;
                     toast(a);
              $("body .pagination li a").trigger("click",records);

                 }else{
                   toast(a);
                   table();
               }


        },
          fail:function(){

          alert("failed");
     }
      });
}

function edit(id){
    $("#submit").text("update");
     $.ajax({
     method:'get',
       url:"http://localhost/nb/transaction/edits/"+id,
     success:function(resp){

             $('#id').val(resp.id);
             $('#member_id').val(resp.member_id);
             $('#amount').val(resp.amount);
             $('#date').val(resp.date);

         },
        fail:function(){

         }
     });

   }


function delt(id){
    var conf=confirm("Are you sure you want to delete?");
   if(conf){
    $.ajax({
        method:'get',
      url:"http://localhost/nb/transaction/deletes/"+id,
        success:function(resp){
             var a =JSON.parse(resp);
             toast(a);
               table();
        },

        fail:function(){
            alert("Fail");

        }
    });
}
}
function resetForm(){
  $('#id').val('');
  $('#submit').text('Create');
}
</script>

<?php include(resource_path().'/views/sections/footer.php');
