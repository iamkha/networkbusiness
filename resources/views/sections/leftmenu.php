<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!--<li class="header">MAIN NAVIGATION</li>-->

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Business Config.</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo url('/company');?>"><i class="fa fa-circle-o"></i>Company-Profile</a></li>
            <li><a href="<?php echo url('/business');?>"><i class="fa fa-circle-o"></i>Business-Configuration</a></li>
              <li><a href="<?php echo url('/level');?>"><i class="fa fa-circle-o"></i>Level</a></li>
             </ul>
        </li>
        <li class="treeview">
        <a href="<?php echo url('/tree');?>" >
          <i class="fa fa-dashboard"></i> <span>Tree</span>
        </a>
      </li>
      <li class="treeview">
      <a href="<?php echo url('/transaction');?>" >
        <i class="fa fa-dashboard"></i> <span>Transaction</span>
      </a>
    </li>
    <li class="treeview">
    <a href="<?php echo url('/memberlist');?>" >
      <i class="fa fa-dashboard"></i> <span>Member List</span>
    </a>
  </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->
