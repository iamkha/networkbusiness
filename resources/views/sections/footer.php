<footer class="main-footer">
<!--    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>-->
    <strong>Copyright &copy; 2014-2016 <a href="#">SAIPAL</a>.</strong> All rights
    reserved.
     <?php include(resource_path().'/views/sections/modal.php');?>
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo asset('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo asset('assets/bootstrap/js/bootstrap.min.js');?>"></script>


<!-- SlimScroll -->
<script src="<?php echo asset('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo asset('assets/plugins/fastclick/fastclick.js');?>"></script>
<script src="<?php echo asset('js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo asset('js/jquery.toast.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo asset('js/app.min.js');?>"></script>
<script src="<?php echo asset('js/pagination.js');?>"></script>
<script src="<?php echo asset('js/scripts.js');?>"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript">
(function(){
    //work for the notifications
    if(typeof notification === "object" && notification !== null){
        if(notification.status==0){
           var icon = "error";
        }
        else if(notification.status==1){
            var icon = "success";
        }
        else if(notification.status==2){
            var icon = "warning";
        }
        else{
            var icon = "";
        }
        var tostObj = {
            heading:notification.title,
            text:notification.text,
            showHideTransition: 'slide',
            position:'top-right',
            icon:icon
        };
        $.toast(tostObj);
    }


})();
    function toast(a){
     var tostObj = {
                heading:a.title,
                text:a.text,
                showHideTransition:'slide',
                 position:'top-right',
                icon:a.title
               };

             $.toast(tostObj);


      return true;
    }

     <?php if('treeConstruct'){ ?>
 if(typeof(treeConstruct)=='function' ){
        treeConstruct();
      }
   <?php } ?>

   <?php if('table'){ ?>
if(typeof(table)=='function' ){
      table();
    }
 <?php } ?>


<?php if(Session::has('fullname') || Session::has('role')):?>
$("#user-fullname").text('<?php echo Session::get('fullname');?>');
$("#user-email").text('<?php echo Session::get('role');?>');
  <?php endif; ?>


</script>
</body>
</html>
