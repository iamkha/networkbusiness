-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2017 at 12:47 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `network`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_config`
--

CREATE TABLE `business_config` (
  `id` int(11) NOT NULL,
  `comission_rate` int(10) NOT NULL,
  `capping_amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_config`
--

CREATE TABLE `company_config` (
  `id` int(10) NOT NULL,
  `company_name` varchar(80) NOT NULL,
  `slogan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `earning_heading`
--

CREATE TABLE `earning_heading` (
  `id` int(10) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `earning_heading`
--

INSERT INTO `earning_heading` (`id`, `name`) VALUES
(1, 'ACCUMULATIVE  BUSINESS COMMISSION '),
(2, 'SUPER FAST ACHIEVER REWARD'),
(3, 'LEVEL AWARDS '),
(4, 'LEADERSHIP BONUS'),
(5, 'TRAVEL FUND'),
(6, 'CAR FUND'),
(7, 'FAMILY WELFARE FUND');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(30) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `dob` varchar(10) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `fathers_name` varchar(50) DEFAULT NULL,
  `marital_status` varchar(15) DEFAULT NULL,
  `spouse_name` varchar(50) DEFAULT NULL,
  `perm_address` varchar(50) DEFAULT NULL,
  `temp_address` varchar(50) DEFAULT NULL,
  `pan_no` varchar(15) DEFAULT NULL,
  `introducer_name` varchar(50) DEFAULT NULL,
  `introducer_id` int(20) DEFAULT NULL,
  `spiller_name` varchar(50) DEFAULT NULL,
  `spiller_id` int(20) NOT NULL,
  `stockist_name` varchar(50) DEFAULT NULL,
  `bank_ac_no` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `mobile` int(12) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `side` int(5) NOT NULL,
  `color` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `fullname`, `dob`, `sex`, `fathers_name`, `marital_status`, `spouse_name`, `perm_address`, `temp_address`, `pan_no`, `introducer_name`, `introducer_id`, `spiller_name`, `spiller_id`, `stockist_name`, `bank_ac_no`, `bank_name`, `mobile`, `path`, `branch`, `side`, `color`) VALUES
(25, 'NetworkBusiness', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '25', '0', 0, 'G'),
(26, 'Hari Bahadur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, '25-26', '0-1', 1, '0'),
(28, 'Ram Bahadur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, '25-28', '0-2', 2, 'G'),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, '25-26-29', '0-1-1', 1, 'G'),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, '25-26-30', '0-1-2', 2, 'G'),
(31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, NULL, NULL, NULL, NULL, '25-26-29-31', '0-1-1-1', 1, '0'),
(36, 'bidhur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, NULL, NULL, NULL, NULL, '25-26-29-36', '0-1-1-2', 2, 'G'),
(37, 'shyam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, '25-26-29-31-37', '0-1-1-1-1', 1, 'R'),
(38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, '25-26-29-31-38', '0-1-1-1-2', 2, 'G'),
(39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38, NULL, NULL, NULL, NULL, '25-26-29-31-38-39', '0-1-1-1-2-2', 2, 'R'),
(40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38, NULL, NULL, NULL, NULL, '25-26-29-31-38-40', '0-1-1-1-2-1', 1, 'R'),
(41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, NULL, NULL, NULL, NULL, '25-26-29-36-41', '0-1-1-2-2', 2, 'G'),
(42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, NULL, NULL, NULL, NULL, '25-26-29-36-42', '0-1-1-2-1', 1, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `rem_bal`
--

CREATE TABLE `rem_bal` (
  `id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `right_balance` int(10) NOT NULL,
  `left_balance` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `amount` int(10) NOT NULL,
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `day` int(2) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `member_id`, `username`, `password`, `role`) VALUES
(1, 25, '25', '12345', 'admin'),
(2, 26, '26', '12345', 'user'),
(3, 28, '28', '12345', 'user'),
(4, 29, '29', '12345', 'user'),
(5, 30, '30', '12345', 'user'),
(6, 31, '31', '12345', 'user'),
(7, 36, '36', '12345', 'user'),
(8, 37, '37', '12345', 'user'),
(9, 38, '38', '12345', 'user'),
(10, 39, '39', '12345', 'user'),
(11, 40, '40', '12345', 'user'),
(12, 41, '41', '12345', 'user'),
(13, 42, '42', '12345', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_config`
--
ALTER TABLE `business_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_config`
--
ALTER TABLE `company_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `earning_heading`
--
ALTER TABLE `earning_heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `rem_bal`
--
ALTER TABLE `rem_bal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_config`
--
ALTER TABLE `business_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_config`
--
ALTER TABLE `company_config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `earning_heading`
--
ALTER TABLE `earning_heading`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `rem_bal`
--
ALTER TABLE `rem_bal`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `rem_bal`
--
ALTER TABLE `rem_bal`
  ADD CONSTRAINT `rem_bal_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
